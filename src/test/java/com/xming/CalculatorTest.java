package com.xming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CalculatorTest {

    static private final double DELTA = 1e-6;

    @Test
    public void testInitialValue() {
        Calculator calculator = new Calculator();
        assertEquals("Checking initial value", 0.0, calculator.calc(), DELTA);
    }

    @Test
    public void testStartingValue() {
        Calculator calculator = new Calculator(10.0);
        assertEquals("Checking starting value", 10.0, calculator.calc(), DELTA);
    }

    @Test
    public void testAddition() {
        Calculator calculator = new Calculator().add(5).add(3);
        assertEquals("Checking addition", 8.0, calculator.calc(), DELTA);
    }

    @Test
    public void testMultiplication() {
        Calculator calculator = new Calculator().add(2).times(3);
        assertEquals("Checking multiplication", 6.0, calculator.calc(), DELTA);
    }

    @Test
    public void testCombinationOfOperations() {
        Calculator calculator = new Calculator(2.0)
            .add(10)
            .times(2)
            .add(5)
            .times(3);
        assertEquals("Checking combination of operations", 87.0, calculator.calc(), DELTA);
    }
}
