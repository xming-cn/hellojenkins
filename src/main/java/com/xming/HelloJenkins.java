package com.xming;

import com.xming.Calculator;

class HelloJenkins {
    public static void main(String[] args) {
        Calculator c = new Calculator(10.0);
        System.out.println(
            c.add(3.0).times(2.0).add(5.0).calc()
        );
    }
}
