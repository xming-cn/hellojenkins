package com.xming;

class Calculator {
    private double current;

    public Calculator() {
        this(0);
    }

    public Calculator(double value) {
        this.current = value;
    }

    public Calculator add(double value) {
        this.current += value;
        return this;
    }

    public Calculator times(double value) {
        this.current *= value;
        return this;
    }

    public double calc() {
        return this.current;
    }
}
